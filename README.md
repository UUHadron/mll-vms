# pandaVMs

Scripts for setting up a collection of useful virtual machines.

## Requirements

- [Vagrant](https://www.vagrantup.com/) (Do not install via package manager! Version too old!)
- [VirtualBox](https://www.virtualbox.org/)

Some of the boxes use a few vagrant plugins. These can be installed through

```sh
./install-vb-plugins.sh
```

## Usage

Go into the corresponding folder for the virtual machine you want to provision, e.g.

```sh
cd mll-dev
```

You can then use the following commands to control the virtual machine.

- `vagrant up` Boot into the virtual machine. If this done for the first time, it will be provisioned automatically.
- `vagrant halt` Shutdown the virtual machine.
- `vagrant suspend` Save the machine state and switch it off afterwards.
- `vagrant resume` Restore a previously suspended virtual machine at its saved state.
- `vagrant ssh` Connect to the virtual machine via an ssh connection.

Note: All virtual machines are created with a user `vagrant` and the password `vagrant`.

## Boxes

### mll-dev

Machine intended for development of or with PandaRoot. Will be automatically provisioned with the following:

- Ubuntu-MATE desktop (reasonably fast GUI when using as VM)
- Visual Studio Code editor
- ROOT v6-14/04
- Docker
- clang-7, clang-tidy-7

Notes: Check `ansible/panda-dev.yml` if you want to make change to the provisioning of the machine. This can include the installation of docker-ce.

## Known issues
